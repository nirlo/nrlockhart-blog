+++
title = "Introduction"
date = "2019-07-16"
tags = ["introduction", "blog", "nrlockhart"]
+++

Yo, how's it hanging? This is the blog portion of my site. 

### What's the point

This is where I am going to place just about about anything I want that is not related to technology.

Stories? Yea, I'll put them here.

Rants? If it is about a show or movie, yea, that will go here.

Cursing? Yea, I'm from coast, I curse like a sailor, get fucking used to it.