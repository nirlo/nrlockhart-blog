+++
title="How to End a Decade"
date="2019-09-01"
tags=["TV", "Game of Thrones", "MCU", "stories"]
+++

A decade ago, three things happened in my life: I became the legal age of majority and left high school as an “Adult”, Iron Man came out, and rumors from my fantasy loving friends was that something was on the horizon. I love pop culture and I have always loved science fiction and fantasy. I grew up reading The Lord of The Rings, watching Battlestar Galactica and Firefly, and I loved the Night Angel trilogy (not that well known, not sure if it holds up). When Iron Man came out, I was elated. The end credits came and the possibilities expanded. 

I don’t have to describe much more about what happened from that moment that Tony Stark said “I am Iron Man”. It spawned a decade of movies, stories that ranged from political thrillers to heists, almost shakespearean family drama to pure 80’s comic book (same trilogy. Gods, I adore Ragnarok). While not every movie was perfect, they never seemed to disappoint. If you were to walk into a Marvel movie with the expectation that you would be at least well entertained, you would walk out satisfied. If you walked in expecting the best movie you had ever seen, a little harder mark to hit, but some reached that point (personal opinion).  

Three years after that famous line being first uttered, something else crept up into the hive mind of pop culture. A slow, winding exploration of politics in a medieval world. Where zombies exist (maybe), dragons existed (again, a maybe), a cripple kid has weird magic dreams, and Sean Bean dies. That last part is what caught everyone (unless you knew the meme of Bean, him acting a character is a death flag). The main character, your view into the world and the hero died. 

HE WAS THE MAIN CHARACTER! WHAT THE FUCK! FUCK YOU JOEFFERY! 

It was harsh, unexpected, and chilling moment that set this world apart from anything else that had been produced for television. Also 5 minutes an episode dedicated to a Littlefinger’s brothel commercial titty break. For the eight years that the show ran, it grew every year. With 2 million viewers in the first year, it tripled by the fourth, and reached over 12 million an episode in the final season. 

Both of these franchises were a cultural phenomena that have changed the way that we consume both television and movies. The MCU became a movie serialized in the same way that television series is. Game of Thrones became television series with the budget to ensure each episode became their own movies. Both are adaptations as well, comic books and a deep dive into medieval cuisine with a writer who still hasn’t finished the series. Yes, Most of the stories that the MCU has used in their movies are completed stories, so comparing them as I am going to be may not be completely fair.
	
Game of Thrones ended terribly. The MCU came to a 10 year long conclusion in the most satisfying way possible.
	
But something is off in this comparison and it simply is not about the completion of the stories that they are adapted from. It’s an argument that I have heard almost too many times since Game of Thrones finished. “You’re upset how it ended because your theory was wrong”. Also “They subverted your expectations. It’s good because of that.”

Those phrases have become an almost call to action by everyone on the internet when someone offers a critique to new media. It happened with Star Wars: The Last Jedi. It happened with Game of Thrones. It's happening with almost any series that does something different.

But I don't think it is a case of theories being wrong. It's a matter of the writing and the interaction with the audience. 

> A good ending is one that the audience expected. A great ending is what they expect and what they missed. 

It has become a plague in writing and stories to subvert the expectations of the audience. There is a hyper focus on the twist. I think this stems from commentary of great stories. Movies like Inception, Arrival, and The Prestiege bent the expectations of the audience and left you questioning the entire movie. It goes back even to older movies, especially M. Night movies. `WHAT A TWIST`. In books, I can think of one great example with the Night Angel Trilogy, which most people will not know. These stories left you in awe of the ending. They left you shaken, but understanding how they reached that point.

Weak writers see a twist as a shortcut to greatness. They see the reactions that the audience has to these twists and what want that reaction. They forget the work that it takes to deserve the twist though. In The Prestiege, when the twist comes, to uses flashbacks to show you the evidence of the twist. Here is where the brothers changed themselves. Here is the character interaction that shows you how it all worked. But you were too busy to notice it. If you were paying enough attention, maybe you would have caught it. But you didn't, so you sit there for a moment and let it all soak in. Your expectations are satied but you are left with more than what you though.

The Game of Thrones writers took what G.R.R.Martin had wrote and set up after years and gracefully adapted it until they had nothing left to adapt. I am sure, that when Martin finally finishes the story, it will be satisfying, but what D&D finished with was a mess. The expectations weren't set up for the ending nor any of the twists. Fan service was just that; it didn't serve any purpose to the plot and felt jarring. It's like when your drunken uncle tells you a joke, hits the punchline and when you don't laugh, he tells you that he forgot to mention the preist walking in. It makes the joke kind of better, but it's too late, the joke is awful.

Then there is the MCU. There have been expectation about how it will end fo years. We all knew that RDJ's contract was ending, along with Chris Evans. Both of these characters had to finsih their tenure. We also knew that they had to undo the snap and defeat Thanos. We had expectations about how Endgame was going to happen. But leaving the theatre, we all got more than we expected. We had an emotional closure for Iron Man with the line that began it all, we got a damn time heist, we got the exit of Captain America, and we got Fat Thor's journey through depression. There were call backs, but they weren't fan service, they were natural. 

There are two key components to a great ending; What you expected and what you didn't expect. Those two parts are not equivient though. You can have the first and the story will be overall mediocre to okay. If you only have the second, you don't have a cohesive story. 

example:
  There once was a mouse. He loved to cook and want to be a chef. He worked everyday at cooking, making every kind of dish that he could. On the day that he went in for an interview, a cat gutted him and left him on a doorstep. 

Neat story, but why did I just spend a couple sentences talking about his cooking if he just ended up dead. It's an absurdists dream story, but that is a tiny niche of an audience. 

How about a better example:
  There once was a mouse. He loved to cook and want to be a chef. He worked everyday at cooking, making every kind of dish that he could. On the day that he went in for an interview, He made the best meal that he ever made, and the head chief loved it but had food poisoning from the mouse fur.

Our expectations about the mouse are satisfied with all of the cooking, but we are surpirsed but the food poisoning. If this was an actual story and I was a better writer, it would be a better story all around.

There are ways to have a twist, but it requires work and set up. You can't cheat it. 

It extends from the shortest of stories all the way to decade spanning epics. Fiege took advice from those around him and guiding the whole. D&D, by all accounts, took advice from no one and only did as they wanted. 

That's my rant for the week. I'm sure I'm wrong about something somewhere in here, but that's like, your opinion man.