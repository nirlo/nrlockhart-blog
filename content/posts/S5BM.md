+++
title = "Striking back at the Vipers"
date = "2019-07-17"
tags = ["Black Mirror", "TV", "VR", "blog", "Science Fiction"] 
+++

It's a ritual. Everytime that I watch a movie or the latest episode of [`insert trending show`], I look online for any discussion. Do my views of what I just consumed align with the online consensus or are my views unique? Is there something that I missed that make the piece of work a little better. 

Black Mirror is fantastic for this. Since season 3, I've delighted in looking for the little details that make these dystopian stories elevated above other anthologies. It sits in a spectrum of science fiction that explores not only the harshest parts of ever driving force forward with technology, but how is it going to affect us at a personal level. That, to me, is what great science fiction should do. It should be about the human part of the story in the face of changing technologies or sometimes the horror of what awaits us. 

So after watching S5E1, I was super excited to see the discussion around the episode. It didn't have the typical bleak ending that most episodes of `Black Mirror` have in the past, but it explored the technology that has been the (kind of) spine of `Black Mirror` from season 3 in `San Junipero` in a unique way. 

But, the discussion seemed to lament the episode. Not everyone was as jazzed as I was about the episode.

Here's a selection of comments in the episode discussion thread on `r/blackmirror`

> God awful episode

> Just finished watching it.
>
>That was easily the worst episode of Black Mirror I've ever seen (though granted I haven't seen them all).

> A piece of crap. Swj nonsense.

> 🎮𝘊𝘢𝘭𝘭 𝘔𝘦 𝘉𝘺 𝘠𝘰𝘶𝘳 𝘎𝘢𝘮𝘦🎮

> Imagine going into the game for the first time only to be met face to face with a horny polar bear.
>
>mount
>
>“Hey! What the...what are you...oh gawd...oh gawd...exit game....EXIT GAME!”

A lot of the more nuanced dsicussion does center around the idea that `Black Mirror` explores the fact that our emotions and our normal hang ups don't change with the technology that surrounds us. And This episode answers likely one of the oldest bro questions

> Bro, I was, like a chick... Would you fuck me?

The episode avoids the typically "dark" contexts that `Black Mirror` spills out and tells a story that would belong in an anthology short story book. 

It's a starts simple enough. Danny and Karl best friends that haven't seen each other in a long time meet at Danny's birthday party. Karl gives him the latest version of the fighting game that they played through late nights a decade earlier and the VR adaptor. 

First thing, VR video game in `Black Mirror` has almost never been a good thing. In `Playtest`, in what seems to be a rudimentary precursor to the VR in this episode, the main character is driven mad by horror images of his minds own creation before the prototype renders him brain dead. In `White Christmas`, A VR simulation is used to torture a confession out of an AI replica of a prisoner. There are lot of bad uses of VR in `Black Mirror`

So, the safe bet with the episode is that some shit is about to go down. Someone is going to feel all of the pain of VR and die in real life, or it's all going to be a simulation after one of them 'dies', or they are always in a simulation. I don't throw away bad ideas when watching sci-fi, cause it could happen. 

There are what seem to be hints of a potentially violent story about to unfold as well. It's short, but when Danny is putting a knife into the dishwasher, it yells at him that he needs to put it sharp end down. Checkov, is that you? Are you whispering sweet violent nothings into the ears of the audience? Who is going to end up tripping in the kitchen and falling on that knife?

Once the two are in the game and they start fighting, they even mention that they can feel every hit. They feel the pain of the game. OOOOOOOOHHHH boi, some shit is about to go down. 

Karl is playing Roxette, a kick ass punk chick played by Pom Klementieff, Mantis from Guardians of the Galaxy. Danny is playing Lance, a Liu Kang stand-in, played by Ludi Lin. Once they start fighting, Karl is wrecking Danny. The fight sequence is perfectly video game style despite being live action, which I found really surprising. No one is talking about it though, which I think is a damn shame really. Danny gets the upper hand, they end up grappling on the ground and the two characters make out.

Wait, what?

Yea, They're making out. 

That's different...

The rest of the episode goes through the affair these two have in the virtual world, with Karl showing a lot more interest in the whole situation.

I was a little disappointed that they never switch characters, but I think that is the point of the episode. 

People seem to have a lot of distaste for the episode. It ends on a relatively happy note, and there is no dark twist to the technolgy that the audience has grown accustom. 

But I think this the strength of the newer episodes. It's not just the dark twists and turns of technology, it focusing more the on, what I think of as, the central, You. The audience. The introspection when the episode is over and how it affects you. 

Given the ability to fully feel an experience that you would normally never be able to, would you lose yourself into the experience? If your best friend was of the opposite gender, would the love you have transend?

The introspection that happens is a part of what makes Science Fiction one of the better genres that exists. It doesn't matter the technolgy, the setting, The greatest sci-fi is the human interaction with the technology.

> The only thing worth writing about is the human heart in conflict with itself. 
>
> \- William Faulkner

The new episodes are different from the rest of what Black mirror has been, but I don't think the new direction is a bad one. They're more personal and introspective. 

The one problem I have with the new episodes is they go against the name. Once the episode is over, it should only be black of the credits, letting the viewer think over what happen. 

But these new eipsodes, have more story after the credits start. It has a message it wants to get across. There is not time to think it through. The impact of the episode is lost, the punchline more like a light feather across the face.