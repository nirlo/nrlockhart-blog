+++
title="10x Spies"
date="2019-09-01"
tags=["TV", "Archer", "Characters", "rant"]
+++

Archer season 10 finally dropped on Netflix. I've loved the show since my roommate in university introduced me to the show in... I think the second season... maybe? The show hooked me right off the bat. The jokes were smart but didn't wait for you to get them. The slapstick was always a sidepiece to the insane characters and their interactions with one another in the mission of the week adventures. As they moved some of the side characters more into the main story stream, you could see them highlighted and how they bounced off one another. Cheryl/Crystal's insanity when matched with Kreiger. Pam in HR slowly becoming more of a badass and how much of a beatdown she could put on the other. 

But as I said, it's been 10 seasons. They had to change things up eventually and they did. They became drug runners for a season, lost the name of their agency to poor brand recognition, were private detectives in LA in a more modern setting and then a classic film noir setting. They went to space and were Indiana Jones style adventures in another season. 

That's a lot of change for a show that started off as a spy show. They hopped genre to keep the interest up. It was good... for the first couple of changes. 

These genre changes were mostly to go over tropes that exist in those genres. The Femme Fatal, the tropical princess, Alien, TNG court rooms. 

But that's about it. They changed the genre only to poke holes at the tropes in the genre but not really add much beyond that. The characters regressed almost completely from early seasons. Gillette was a good spy in the first three seasons, but was reduced entirely to a stereotype and accident prone. Each season had him losing then regaining the use of his legs. Great laughs for the first couple of times it happened, but it definitely overstayed it's welcome. 

Pam started as a joke on HR employees, became a boss, and then was reduced to bad sex jokes.

Cheryl remained insane, but her reasons for being insane became less clear and defined and the charm wore off.

Archer himself had some growth up until the Noir season. 

I get it narratively though, the last three seasons have been coma dreams. The characters are constructs of Archer's mind as he sleeps. But it really comes across that they don't seem to know what to do with these characters anymore and coma dream is a terrible excuse for bad characters. 

On top of that, the show has become a trope of itself. It's formulaic to early seasons. Lana has a plan, Archer screws it up, makes a joke, Mallory is drinking and makes a quip about them being idoits, Ciryl is a coward and yells suppressing fire, Cheryl is bored and yells about her supervisor, Gillette is gay and in the background, and Pam talks about something overtly sexual or farts. The mission is successful despite everything and Archer chuckles while making an obscure reference. <sup>danger zone<sup>.

There are still callbacks, but it feels less natural and feels like scraping the bottom of the barrel for fan service. *Hey, remember this joke! Remember the ocelot! Remember Seamus! You found it funny the first time! Right!*

I can tell they're trying to reinvigorate the show by switching it up, but it's been 5 seasons of *switching up*. They've only changed the genre and almost nothing else. It's become stale. I quickly watched this season but wasn't paying as much attention as I have early seasons. I haven't even rewatched any season past season 5. The formula has dried up, your baby is sucking on dry nestle branded, none-nutritional dust.

I still rewatch the first seasons every once in awhile because the jokes feel fresh in the context. 

From what I have heard of season 11, which was not expected to happen from my understanding, it looks like they will change things around. Maybe the characters have grown outside of the coma dreams and have become more rounded. At the moment, they're more 2-dimensional then the animation. 

That's my feeling though. I do feel that the show has run it's course. If they can, they should leave it off on an interesting note with season 11 and leave it there. Dinner's over, go home, and enjoy the time that we had instead of milking an extra hour out of the visit.

But this is just a rant of mine. I'm not sure how they will change things in the future, but it's an interesting case of becoming generic while changing genres. Maybe another show like Archer will appear, maybe not. but the first 5 are still a go to and awesome in my books.