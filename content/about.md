+++
title = "About"
date = "2019-06-30"
+++

I am a DevOps Engineer at Fullscript. I mostly work with Kubernetes, but a majority of my job is finding new ways of describing infrastructure to developers and beyond. Metrics turned into visualizations that can be easily parsed, or articles on how to use a new piece of the system. Teaching not only developers but analysts how to use some of the more complicated systems that exist in the DevOps sphere. Mostly Kubernetes, because that is a beast that takes some getting used to in it's structure. 